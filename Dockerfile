FROM python:3.8-alpine

RUN /sbin/apk add --no-cache --virtual .deps gcc musl-dev \
 && /usr/local/bin/pip install --no-cache-dir black==19.10b0 \
 && /sbin/apk del --no-cache .deps

RUN pip install mkdocs
RUN pip install mkdocstrings
RUN pip install markdown
RUN pip install pymdown-extensions
RUN pip install mkdocs-material
RUN pip install mkdocs-git-revision-date-localized-plugin

ENV PYTHONUNBUFFERED True

WORKDIR /opt/
COPY . .
